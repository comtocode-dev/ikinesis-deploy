import { startStimulusApp } from '@symfony/stimulus-bridge';
// Update : https://github.com/symfony/stimulus-bridge#common-errors
// Related to error : Module build failed: Module not found:
// ./assets/bootstrap.js contains a reference to the file @symfony/autoimport.
// import '@symfony/autoimport';

// Registers Stimulus controllers from controllers.json and in the controllers/ directory
export const app = startStimulusApp(require.context('./controllers', true, /\.(j|t)sx?$/));
