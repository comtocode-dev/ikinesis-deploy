import {Controller} from "stimulus"

export default class extends Controller {
    connect() {
        this.handleScroll();
    }

    handleScroll() {
        // Variables
        let bodyHeight = document.body.offsetHeight;
        let bottomPositionToLoadItems = 250;
        let currentBottomPosition = bodyHeight - bottomPositionToLoadItems;
        let scrollStatus = 0;
        let spinnerContainer = document.getElementById('spinner-container');
        // Basically hide the spinner container
        spinnerContainer.style.display = "none";

        window.loadItems = this.loadItems;
        window.generateJsonPath = this.generateJsonPath;
        window.getUrlParams = this.getUrlParams;

        // Add an event listener to listen scroll action
        document.addEventListener('scroll', function () {
            if (scrollStatus === 0) {
                // If total window size is >= than the current bottom position into window
                // We can load new items
                if ((window.scrollY + window.innerHeight) >= (currentBottomPosition)) {
                    window.loadItems(
                        jsonPath,
                        'block-list',
                        locationId,
                        contentTypeDisplayFull
                    );
                    // We update the value of the body's height (to avoid reload at the wrong place)
                    bodyHeight = document.body.offsetHeight;
                    // We update the value of the current bottom position
                    // in the window according to the new body's height
                    currentBottomPosition = bodyHeight - bottomPositionToLoadItems;
                    // We increment the scroll status to avoid the repeat of the action each time we scroll
                    scrollStatus++;
                }
            } else {
                // We update the value of the body's height (to avoid reload at the wrong place)
                bodyHeight = document.body.offsetHeight;
                currentBottomPosition = bodyHeight - bottomPositionToLoadItems;

                // If we are not into the right place of the window,
                // we put scroll status back to its initial value
                if ((window.scrollY + window.innerHeight) <= (currentBottomPosition)) {
                    scrollStatus = 0;
                }
            }
        });
    }

    loadItems(jsonPath, divId, currentLocationId, contentTypeDisplayFull) {
        let listElement = document.getElementById(divId);
        let spinnerContainer = document.getElementById('spinner-container');
        spinnerContainer.style.display = "none";

        if(listElement.getAttribute('data-page') !== "end"){
            // Variables
            let page = parseInt(listElement.getAttribute('data-page'));
            let jsonUrl = this.generateJsonPath(jsonPath, page, currentLocationId, contentTypeDisplayFull);
            let nextPage = page + 1;

            // Show spinner container
            if (spinnerContainer.style.display === "none") {
                spinnerContainer.style.display = "flex";
            }
            listElement.setAttribute('data-page', "" + nextPage);

            // Create a getJSON Function
            let getJSON = function (url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.responseType = 'json';
                xhr.onload = function () {
                    let status = xhr.status;
                    if (status === 200) {
                        callback(null, xhr.response);
                    } else {
                        callback(status, xhr.response);
                    }
                };
                xhr.send();
            };

            // Call it
            getJSON(jsonUrl, function (err, data) {
                let htmlContent = data.html;
                listElement.insertAdjacentHTML('beforeend', htmlContent);
                if (data.loadMoreItems !== true) {
                    listElement.setAttribute('data-page', "end");
                }
            });
        } else {
            spinnerContainer.style.display = "none";
        }
    }

    generateJsonPath(path, page, currentLocationId, contentTypeDisplayFull) {
        return path + '/' + currentLocationId + '/' + contentTypeDisplayFull + "/" + page + '?' + this.getUrlParams();
    }

    getUrlParams() {
        let url = window.location.href;
        let res = url.split("?");
        if (res.length >= 2)
            return res[1];
        else
            return '';
    }
}