import {Collapse} from "bootstrap/js/src/collapse"
import {Controller} from "stimulus"

export default class extends Controller {
    connect() {
        let navbarCollapseMenu = document.getElementById('navbar-nav-dropdown');
        let bodyOverlay = document.getElementById('nav-overlay');

        // Add event listener on navbar closing
        navbarCollapseMenu.addEventListener('show.bs.collapse', function () {
            // Add class "lock-scroll" class to the body when menu is opening
            document.body.classList.add("lock-scroll");
            // Add class "shown" class to the overlay when menu is opening
            bodyOverlay.classList.add("shown");
        })
        // Add event listener on navbar closing
        navbarCollapseMenu.addEventListener('hide.bs.collapse', function () {
            // Remove class "lock-scroll" class to the body when menu is closing
            document.body.classList.remove("lock-scroll");
            // Remove class "shown" class to the overlay when menu is closing
            bodyOverlay.classList.remove("shown");
        })
    }

    hideNav() {
        let navbarCollapseMenu = document.getElementById('navbar-nav-dropdown');
        let navbarToggler = document.getElementById('toggler');
        let navbarTogglerAttr = navbarToggler.getAttribute("aria-expanded");

        // Remove class "lock-scroll" class to the body when menu is closing
        document.body.classList.remove("lock-scroll");
        // Remove class "shown" of the element
        this.element.classList.remove('shown');
        // Remove class "show" of the navbar collapse menu
        navbarCollapseMenu.classList.remove('show');
        // Remove attribute on navbar toggler
        if (navbarTogglerAttr === 'true') {
            navbarToggler.setAttribute("aria-expanded", "false");
        }
    }
}