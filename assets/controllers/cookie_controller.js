import {cookieconsent} from "cookieconsent"
import {Controller} from "stimulus"

export default class extends Controller {
    connect() {
        this.initCookie();
    }

    initCookie() {
        window.cookieconsent.initialise({
            container: document.getElementById("rgpd"),
            content: {
                policy: cookieTitle,
                message: cookieMessage,
                allow: cookieAccept,
                deny: cookieDeny,
                link: cookiePrivacyLinkTitle,
                href: cookiePrivacyLink,
            },
            domain: "ikines.is",
            position: "bottom-right",
            theme: "block",
            type: "opt-out",
            palette: {
                popup: {
                    background: "#7d8dc4",
                    text: "#ffffff"
                },
                button: {
                    background: "#8bc97e",
                    text: "#ffffff"
                },
                highlight: {
                    background: "#414e78",
                    text: "#ffffff"
                }
            },
            revokable: true,
            onInitialise: function (e) {
                this.options.type;
                var t = this.hasConsented();
                "allow" == e && t && gaEnable();
            },
            onStatusChange: function (e, t) {
                this.options.type;
                var n = this.hasConsented();
                "allow" == e && n ? gaEnable() : gaDisable();
            }
        });

        function deleteCookies(e) {
            for (var t = 0; t < e.length; t++) document.cookie = e[t] + "=; expires=Thu, 01 Jan 2000 00:00:00 GMT; path=/;", document.cookie = e[t] + "=; expires=Thu, 01 Jan 2000 00:00:00 GMT; path=/; domain=." + location.hostname + ";", document.cookie = e[t] + "=; expires=Thu, 01 Jan 2000 00:00:00 GMT; path=/; domain=." + location.hostname.split(".").slice(-2).join(".") + ";"
        }

        function addScript(e, t) {
            var n = document.createElement("script");
            t && (n.id = t), n.src = e, n.async = !0, document.body.appendChild(n)
        }

        function addInlineScript(e, t) {
            var n = document.createElement("script");
            t && (n.id = t), n.textContent = e, document.body.appendChild(n)
        }

        function removeElement(e) {
            var t = document.getElementById(e);
            t && t.remove()
        }

        function gaEnable() {
            addInlineScript("window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date; ga('create', '" + gaUa + "', 'auto');ga('send','pageview')", "gaInlineScript"), addScript("//www.google-analytics.com/analytics.js", "gaScript", !0)
        }

        function gaDisable() {
            let e = new Date;
            let gaCookies = ["_ga", "_gat", "_gid"];
            e.setTime(e.getTime() + 31536e6), document.cookie = "cookieconsent_status=deny;path=/;expires=" + e.toGMTString(), deleteCookies(gaCookies), removeElement("gaInlineScript"), removeElement("gaScript")
        }
    }
}