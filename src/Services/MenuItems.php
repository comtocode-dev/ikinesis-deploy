<?php

namespace App\Services;

use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\Routing\Generator\UrlAliasGenerator;


class MenuItems
{

    /**
     * @var SearchService
     */
    protected $searchService;

    /**
     * @var ConfigResolverInterface
     */
    protected $configResolver;

    /**
     * @var ContentService
     */
    protected $contentService;

    /**
     * @var LocationService
     */
    protected $locationService;

    /** @var UrlAliasGenerator */
    private $urlAliasGenerator;

    /**
     * MenuItems constructor.
     * @param SearchService $searchService
     * @param ConfigResolverInterface $configResolver
     * @param ContentService $contentService
     * @param LocationService $locationService
     * @param UrlAliasGenerator $urlAliasGenerator
     */
    public function __construct(
        SearchService $searchService,
        ConfigResolverInterface $configResolver,
        ContentService $contentService,
        LocationService $locationService,
        UrlAliasGenerator $urlAliasGenerator
    )
    {
        $this->searchService = $searchService;
        $this->configResolver = $configResolver;
        $this->contentService = $contentService;
        $this->locationService = $locationService;
        $this->urlAliasGenerator = $urlAliasGenerator;
    }

    /**
     * @param $options
     * @param array $contextArray
     * @param int $mainLocation
     * @return array
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotImplementedException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function createMenu($options, $contextArray = [], $mainLocation = 2)
    {
        $defaultItemClass = (isset($options['itemClass'])) ? $options['itemClass'] : 'nav-item';
        $linkClass = (isset($options['linkClass'])) ? $options['linkClass'] : 'nav-link';
        $currentClass = (isset($options['currentClass'])) ? $options['currentClass'] : 'active';
        $currentLocationId = (isset($options['currentLocationId'])) ? $options['currentLocationId'] : 2;
        $currentLocation = $this->locationService->loadLocation($currentLocationId);
        $currentLocationMainItem = $currentLocation->path;
        $customTextItem = (isset($options['customTextItem'])) ? $options['customTextItem'] : null;
        $rootLocation = $this->locationService->loadLocation($mainLocation);

        $queryArray = [];
        $queryArray[] = new Criterion\ParentLocationId($mainLocation);

        if (isset($contextArray['content_type']) && count($contextArray['content_type']) > 0) {
            $queryArray[] = new Criterion\ContentTypeIdentifier($contextArray['content_type']);
        }
        if (isset($contextArray['section']) && count($contextArray['section']) > 0) {
            $queryArray[] = new Criterion\SectionIdentifier($contextArray['section']);
        }

        if (isset($contextArray['exclude_lrid']) && count($contextArray['exclude_lrid']) > 0) {
            $queryArray[] = new Criterion\LogicalNot(
                new Criterion\LocationRemoteId($contextArray['exclude_lrid'])
            );
        }


        $query = new LocationQuery();
        $query->filter = new Criterion\LogicalAnd($queryArray);
        $query->sortClauses = $rootLocation->getSortClauses();

        $menuItems = $this->searchService->findLocations($query)->searchHits;

        $return = [];
        if (count($menuItems) > 0) {
            foreach ($menuItems as $menuItem) {
                $menuItemLocation = $menuItem->valueObject;
                $menuItemName = $menuItemLocation->contentInfo->name;
                $itemClass = (in_array($menuItemLocation->id, $currentLocationMainItem)) ? $defaultItemClass . " " . $currentClass : $defaultItemClass;
                $return[$menuItemName] = [
                    'uri' => $this->urlAliasGenerator->doGenerate($menuItemLocation, []),
                    'attributes' => ['class' => $itemClass],
                    'linkAttributes' => [
                        'class' => $linkClass,
                        'title' => $menuItemName
                    ],
                    'currentLocationId' => $menuItemLocation->id
                ];
            }
        }

        if (!is_null($customTextItem)) {
            foreach ($customTextItem as $item) {
                $return[$item] = [
                    'attributes' => ['class' => $itemClass],
                ];
            }
        }
        return $return;
    }
}

