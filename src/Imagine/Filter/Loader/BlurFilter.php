<?php

namespace App\Imagine\Filter\Loader;

use Imagine\Image\ImageInterface;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

class BlurFilter implements LoaderInterface
{

    /**
     * @param ImageInterface $image
     * @param array $options
     * @return ImageInterface
     */
    public function load(ImageInterface $image, array $options = [])
    {
        $blur = (!empty($options)) ? $options[0] : 10;
        $image->effects()->blur($blur);

        return $image;
    }

}

