<?php

namespace App\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;
use Netgen\TagsBundle\API\Repository\TagsService;
use Netgen\TagsBundle\API\Repository\Values\Content\Query\Criterion\TagId as TagIdCriterion;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{

    /**
     * @var SearchService
     */
    protected $searchService;

    /**
     * ArticleController constructor.
     * @param SearchService $searchService
     */
    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * @param ContentView $view
     * @param Request $request
     * @param TagsService $tagsService
     * @return ContentView
     */
    public function full(ContentView $view, Request $request, TagsService $tagsService)
    {
        $tags = $view->getContent()->getFieldValue('tags');
        $tagsArray = [];
        if (count($tags->tags) > 0) {
            foreach ($tags->tags as $tag) {
                $tagsArray[] = $tag->id;
            }
        }
        $query = new Query();
        // 4 - Current article
        $query->limit = 4;
        $criterion = [];
        $criterion[] = new Criterion\Visibility(Criterion\Visibility::VISIBLE);
        $criterion[] = new Criterion\Subtree("/1/2/54/");
        $criterion[] = new Criterion\ContentTypeIdentifier(['article']);
        if (count($tagsArray) > 0) {
            $criterion[] = new TagIdCriterion($tagsArray);
        }
        $query->query = new Criterion\LogicalAnd($criterion);

        $relatedContent = $this->searchService->findContent($query);

        $view->addParameters([
            'items' => ($relatedContent->totalCount > 0) ? $relatedContent->searchHits : []
        ]);
        return $view;
    }

}
