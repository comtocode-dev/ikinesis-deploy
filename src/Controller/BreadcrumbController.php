<?php

namespace App\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Exceptions\InvalidArgumentException;
use eZ\Publish\API\Repository\Exceptions\NotFoundException;
use eZ\Publish\API\Repository\Exceptions\UnauthorizedException;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Symfony\Component\HttpFoundation\Response;

class BreadcrumbController extends Controller
{
    /**
     * @var LocationService
     */
    private $locationService;
    /**
     * @var SearchService
     */
    private $searchService;

    public function __construct(LocationService $locationService, SearchService $searchService)
    {
        $this->locationService = $locationService;
        $this->searchService = $searchService;
    }

    /** Load breadcrumb
     * @param int $locationId
     * @return Response
     * @throws InvalidArgumentException
     * @throws NotFoundException
     * @throws UnauthorizedException
     */
    public function load($locationId = 2, $breadcrumbPath = [])
    {
        $breadcrumbs = [];
        if (count($breadcrumbPath) > 0) {
            $countDepth = 1;
            foreach ($breadcrumbPath as $key => $val) {
                if (!is_null($val) && is_numeric($val)) {
                    $loc = $this->locationService->loadLocation($val);
                    $breadcrumbs[$loc->depth] = $loc;
                } else {
                    $breadcrumbs[$countDepth] = $val;
                }
                $countDepth = $countDepth + 1;
            }
        } else {
            $query = new LocationQuery();
            $query->query = new Criterion\Ancestor([$this->locationService->loadLocation($locationId)->pathString]);
            $results = $this->searchService->findLocations($query);
            foreach ($results->searchHits as $searchHit) {
                $breadcrumbs[$searchHit->valueObject->depth] = $searchHit->valueObject;
            }
            ksort($breadcrumbs);
        }
        return $this->render('@ezdesign/includes/common/breadcrumb.html.twig', [
                'breadcrumbs' => $breadcrumbs
            ]
        );
    }

}
