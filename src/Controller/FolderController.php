<?php

namespace App\Controller;

use App\Criteria\Children;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Exceptions\InvalidArgumentException;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;
use eZ\Publish\Core\Pagination\Pagerfanta\ContentSearchAdapter;
use Netgen\TagsBundle\API\Repository\TagsService;
use Netgen\TagsBundle\TagsServiceAwareInterface;
use Pagerfanta\Pagerfanta;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

class FolderController extends Controller
{

    /**
     * @var SearchService
     */
    protected $searchService;

    /**
     * @var LocationService
     */
    protected $locationService;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var ConfigResolverInterface
     */
    protected $configResolver;

    /**
     * @var Children
     */
    protected $childrenCriteria;

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * FolderController constructor.
     * @param SearchService $searchService
     * @param LocationService $locationService
     * @param ConfigResolverInterface $configResolver
     * @param Children $childrenCriteria
     */
    public function __construct(
        SearchService $searchService,
        LocationService $locationService,
        ConfigResolverInterface $configResolver,
        Children $childrenCriteria,
        Environment $twig,
        ParameterBagInterface $parameterBag
    )
    {
        $this->searchService = $searchService;
        $this->locationService = $locationService;
        $this->configResolver = $configResolver;
        $this->childrenCriteria = $childrenCriteria;
        $this->parameterBag = $parameterBag;
        $this->twig = $twig;
    }

    /**
     * @param ContentView $view
     * @param int $depth
     * @param int $limit
     * @param array $contentTypeDisplayFull
     * @return ContentView
     */
    public function showAction(ContentView $view, Request $request, $depth = 1, $limit = 25, $contentTypeDisplayFull = [], TagsService $tagsService)
    {

        $currentTagId = ($request->get('tagId')) ? $request->get('tagId') : false;
        $currentTag = false;
        if($currentTagId){
            $currentTag = $tagsService->loadTag($currentTagId);
        }

        $parentTag = $tagsService->loadTag(1);
        $tags = $tagsService->loadTagChildren($parentTag);
        $tagArray = [];
        foreach($tags as $tag){
            $count = $tagsService->getRelatedContentCount($tag);
            if($count > 0){
                $tagArray[$tag->id] = $count;
            }
        }
        arsort($tagArray);
        $tagDetailsArray = [];
        foreach($tagArray as $tagId => $tagCount){
            $tag = $tagsService->loadTag($tagId);
            $tagDetailsArray[$tag->id]['isActive'] = ($tag->id == $currentTagId) ? true : false;
            $tagDetailsArray[$tag->id]['count'] = $tagCount;
            $tagDetailsArray[$tag->id]['content'] = $tag;
        }

        $items = $this->fetchItems($view->getLocation(), $contentTypeDisplayFull, $limit, $currentTagId);
        $page = ($request->get('page')) ? $request->get('page') : 1;
        $paginator = new Pagerfanta(
            new ContentSearchAdapter($items, $this->searchService)
        );
        $paginator->setMaxPerPage($this->parameterBag->get("list.limit"));
        $paginator->setCurrentPage($page);

        // Add pager fanta
        $view->addParameters([
            'items' => $paginator,
            'haveToPaginate' => $paginator->haveToPaginate(),
            'totalCount' => $paginator->getNbResults(),
            'limit' => $limit,
            'tagDetailArray' => $tagDetailsArray,
            'currentTag' => $currentTag,
            'contentTypeDisplayFull' => $contentTypeDisplayFull,
        ]);
        return $view;
    }

    /**
     * @param $locationId
     * @param $type
     * @param $page
     * @return JsonResponse
     * @throws InvalidArgumentException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function listApi($locationId, $type, $page, Request $request)
    {
        $currentTagId = ($request->get('tagId')) ? $request->get('tagId') : false;
        $location = $this->locationService->loadLocation($locationId);
        $items = $this->fetchItems($location, [$type], 0, $currentTagId);

        $paginator = new Pagerfanta(
            new ContentSearchAdapter($items, $this->searchService)
        );
        $paginator->setMaxPerPage($this->parameterBag->get("list.limit"));
        $paginator->setCurrentPage($page);

        $content = $this->twig->render('@ezdesign/fragment/ajax_list_result.html.twig', [
            'resultList' => $paginator
        ]);

        $offset = ($page * $this->parameterBag->get("list.limit"));
        return new JsonResponse([
            'loadMoreItems' => ($paginator->getNbResults() <= $offset ) ? false : true,
            'html' => $content
        ]);
    }

    /**
     * @param $location
     * @param $contentTypeDisplayFull
     * @param $limit
     * @param $tagId
     * @return array
     */
    private function fetchItems($location, $contentTypeDisplayFull, $limit, $tagId)
    {
        $languages = $this->configResolver->getParameter('languages');
        $query = new Query();

        $query->query = $this->childrenCriteria->generateChildCriterion($location, $languages, $contentTypeDisplayFull, $tagId);
        $query->performCount = false;
        $query->limit = ($limit == 0) ? 100000 : $limit;
        $query->sortClauses = $location->getSortClauses();
        return $query;
    }

}
