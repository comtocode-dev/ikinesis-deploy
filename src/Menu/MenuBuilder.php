<?php

namespace App\Menu;

use App\Services\MenuItems;
use eZ\Publish\API\Repository\Exceptions\InvalidArgumentException;
use eZ\Publish\API\Repository\Exceptions\NotFoundException;
use eZ\Publish\API\Repository\Exceptions\NotImplementedException;
use eZ\Publish\API\Repository\Exceptions\UnauthorizedException;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MenuBuilder
{
    private $factory;

    /** @var ConfigResolverInterface */
    private $configResolver;

    /** @var MenuItems */
    private $menuItems;
    private $parameterBag;

    public function __construct(
        FactoryInterface $factory,
        ConfigResolverInterface $configResolver,
        MenuItems $menuItems,
        ParameterBagInterface $parameterBag
    )
    {
        $this->factory = $factory;
        $this->configResolver = $configResolver;
        $this->menuItems = $menuItems;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param array $options
     * @return ItemInterface
     * @throws InvalidArgumentException
     * @throws NotFoundException
     * @throws NotImplementedException
     * @throws UnauthorizedException
     */
    public function mainMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'navbar-nav');

        $subItemContextArray = $this->subItemContext('main');

        $menuItems = $this->menuItems->createMenu(
            $options,
            $subItemContextArray,
            $this->configResolver->getParameter('content.tree_root.location_id')
        );

        if (count($menuItems) > 0) {
            foreach ($menuItems as $itemName => $itemParams) {
                $menu->addChild($itemName, $itemParams);
            }
        }
        return $menu;
    }

    /**
     * @param $menuContext
     * @return array
     */
    private function subItemContext($menuContext): array
    {
        $subItemContextArray = [];
        $subItemContextArray['content_type'] = ($this->parameterBag->has('menu.' . $menuContext . '.content_type')) ? $this->parameterBag->get('menu.' . $menuContext . '.content_type') : null;
        $subItemContextArray['exclude_lrid'] = ($this->parameterBag->has('menu.' . $menuContext . '.exclude_lrid')) ? $this->parameterBag->get('menu.' . $menuContext . '.exclude_lrid') : null;
        $subItemContextArray['section'] = ($this->parameterBag->has('menu.' . $menuContext . '.section')) ? $this->parameterBag->get('menu.' . $menuContext . '.section') : null;

        return $subItemContextArray;
    }

    /**
     * @param array $options
     * @return ItemInterface
     * @throws InvalidArgumentException
     * @throws NotFoundException
     * @throws NotImplementedException
     * @throws UnauthorizedException
     */
    public function footerMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'list-unstyled');
        $subItemContextArray = $this->subItemContext('footer');
        $menuItems = $this->menuItems->createMenu(
            $options,
            $subItemContextArray,
            $this->configResolver->getParameter('content.tree_root.location_id')
        );

        if (count($menuItems) > 0) {
            $itemParams['attributes']['class'] = 'list-inline-item';
            if (isset($options['main_item'])) {
                $menu->addChild($options['main_item'], $itemParams);
            }

            foreach ($menuItems as $itemName => $itemParams) {
                $itemParams['attributes']['class'] = 'list-inline-item';
                $menu->addChild($itemName, $itemParams);

            }
        }
        return $menu;
    }
}

