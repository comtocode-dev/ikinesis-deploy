<?php

namespace App\Criteria;

use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use Netgen\TagsBundle\API\Repository\Values\Content\Query\Criterion\TagId as TagIdCriterion;

class Children
{
    /**
     * Generate criterion list to be used to fetch sub-items.
     *
     * @param Location $location location of the root
     * @param string[] $languages array of languages
     * @param array $contentType
     * @param int $tagId
     * @return Criterion
     */
    public function generateChildCriterion(Location $location, array $languages = [], array $contentType = [], $tagId = false)
    {
        $criterion = [];
        $criterion[] = new Criterion\Visibility(Criterion\Visibility::VISIBLE);
        $criterion[] = new Criterion\Subtree($location->pathString);
        $criterion[] = new Criterion\LanguageCode($languages);
        if (count($contentType) > 0) {
            $criterion[] = new Criterion\ContentTypeIdentifier($contentType);
        }
        if($tagId){
            $criterion[] = new TagIdCriterion($tagId);
        }
        return new Criterion\LogicalAnd($criterion);
    }
}
